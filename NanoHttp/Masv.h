#ifndef MASV_H
#define MASV_H

#include <string>
#include <memory>
#include "../config/Config.h"
#include "../strLib/Str.h"
#include "MasvMethods.h"


using namespace std;

class Masv {
public:
    Masv();
    Masv(const Masv& orig);
    virtual ~Masv();
    void getData(string &data){
//      string tags = s.subString(data, s.)
        size_t beginTag = 0;
        size_t endTag = 0;
        vector<string> tags;
        string buffer;

        for (int i = 0; i < data.length(); i++) {
            buffer += data.at(i);
            if (buffer.find("<@#") != string::npos){
                buffer.erase(0, buffer.find("<@#"));
                beginTag = buffer.find("<@#")!= string::npos;
                endTag = buffer.find("@/>")!= string::npos;
                if ((beginTag == 1) & (endTag ==1)){
                    tags.push_back(buffer.substr(3, buffer.length() -6));
                    buffer.clear();
                }
            }
        }
        MasvMethods mv;
        for (int i = 0; i < tags.size(); i++) {
            //if (cfg.GetDebug() == 3) { cout << tags[i] << endl; };
            //cout << tags[i] << endl;
            data =  s.replace(data, "<@#"+tags[i]+"@/>", mv.getData(tags[i]));
        }
        
    }
    string getDate(){
        return s.getTimestamp();
    }
private:
    Config cfg;
    Str s;
};

#endif /* MASV_H */

