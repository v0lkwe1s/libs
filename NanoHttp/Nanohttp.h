#ifndef NANOHTTP_H
#define NANOHTTP_H

#include "base64.h"
#include "MType.h"
#include "../strLib/Str.h"
#include "Auth.h"
#include "../config/Config.h"

class Nanohttp {
public:
    Nanohttp();
    Nanohttp(const Nanohttp& orig);
    Nanohttp(string& data);
    string shttp(string data);
    string route(string data);
    string head(string data);
    string post(string data);
    string get(string data);
    string put (string data);
    string delete_ (string data);
    string connect(string data);
    string getMimeType();
    string rest();
    string base64_decript(string encoded);
    string base64_encript(string decoded);
    void sendFile();
    void genericHeader();
    string getWebRootDirectory() const;
    void setWebRootDirectory(string webRootDirectory);
    
    virtual ~Nanohttp();
  
private:
    Str s;
    void error(){
        this->data = "HTTP/1.1 404 OK\r\n";
	this->data += "Access-Control-Allow-Headers: Content-Type, X-Requested-With \r\n";
	this->data += "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS \r\n";
	this->data += "Access-Control-Allow-Origin: * \r\n";
	this->data += "Content-Type: text/plain \r\n";
	this->data += "Connection: close\r\n\r\n";
        this->data += "ERROR 404 - NOT FOUND";
    }
    vector<string> line;
    string data;
    string path;
    Config cfg;
    Auth auth;
    string header;
    const string close = "Connection: close\r\n\r\n";
};

#endif /* NANOHTTP_H */