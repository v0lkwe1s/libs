#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/Masv.o \
	${OBJECTDIR}/MasvMethods.o \
	${OBJECTDIR}/Nanohttp.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-lpqxx -lpq -lpthread -g --std=c++11 -lcrypto -lssl -fPIC
CXXFLAGS=-lpqxx -lpq -lpthread -g --std=c++11 -lcrypto -lssl -fPIC

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libnanohttp.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libnanohttp.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libnanohttp.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libnanohttp.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libnanohttp.a

${OBJECTDIR}/Masv.o: Masv.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -include ../Postgre/PostgreSql.h -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Masv.o Masv.cpp

${OBJECTDIR}/MasvMethods.o: MasvMethods.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -include ../Postgre/PostgreSql.h -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/MasvMethods.o MasvMethods.cpp

${OBJECTDIR}/Nanohttp.o: Nanohttp.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -include ../Postgre/PostgreSql.h -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/Nanohttp.o Nanohttp.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
