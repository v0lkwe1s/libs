#include <string>

#include "Nanohttp.h"
#include "Masv.h"

using namespace std;
bool debug = false;
bool authorization = false;

Nanohttp::Nanohttp(){}
Nanohttp::Nanohttp(const Nanohttp& orig){}
Nanohttp::Nanohttp(string& data){}

string Nanohttp::shttp(string data) {
    this->data = data;
//    
//    if (cfg.requireAuth()) {
//        Auth a;
//        if (a.verifyCredentials(data) < 1) {
//            return a.data;
//        }
//    }
    this->path = s.getUrlPath(data);
    if ((this->path[strlen(this->path.c_str()) - 1]) == '/'){
        this->path += "index.html";
    }
    data ="";
    if (rest() == "rest") return this->data;
        if (this->path == "") this->path = "index.html"; //path não pode ser vazio
        if (this->data.find("POST") != std::string::npos) { data = post(this->data);}
        if (this->data.find("GET") != std::string::npos) { 
            data = get(this->data);
            string found = "HTTP/1.1 404 OK\r\n";
            if (data.find(found.c_str()) != string::npos) {
                cout << data << endl;
                return data;
            }
        }
        else if (this->data.find("HTTP/1.1") != std::string::npos) { data = get(this->data);
    } else {
        data = "HTTP/1.1 501 Not Implemented\r\n";
        data += "Connection: close\r\n\r\n";
        data += "Not Implemented";
    }
    
    genericHeader();
    header += "Content-Length: " + s.numberToStr(data.length())  +  " \r\n";
    header += close;
    string retData = header + data;
    return retData;
}
string Nanohttp::rest(){
	if (debug) { cout << "rest --->" ;}
        return this->data;
}
string Nanohttp::put(string data){return "";}
string Nanohttp::post(string data){
        genericHeader();
        this->data += data;
	return this->data;
}
string Nanohttp::head(string data){return "";}
string Nanohttp::getMimeType(){
	if (debug) { cout << "getMimeType --->" ;}
	string ret = "text/plain";
	vector<MType> mts;
	MType m;
	m.SetExt(".css");
	m.SetType("text/css");
	mts.push_back(m);
	m.SetExt(".htm");
	m.SetType("text/html");
	mts.push_back(m);
	m.SetExt(".json");
	m.SetType("text/plain");
	mts.push_back(m);
	m.SetExt(".ico");
	m.SetType("image/x-icon");
	mts.push_back(m);
	m.SetExt(".jpg");
	m.SetType("image/jpeg");
	mts.push_back(m);	
        m.SetExt(".woff2");
	m.SetType("application/font-woff2");
	mts.push_back(m);
        m.SetExt(".ttf");
	m.SetType("application/x-font-TrueType");
	mts.push_back(m);
        
	for (unsigned int i = 0; i < mts.size(); i++) {
		if (this->path.find(mts[i].GetExt()) != std::string::npos) {
			return mts[i].GetType();
		}
	}
	return ret;
}
string Nanohttp::get(string data){
	if (route(data) == "route") return this->data;
	string file = s.getFileText(cfg.GetWebRootDirectory()+ "/" + this->path);
        if (file.find("<@#") != std::string::npos){
            Masv masv;
            masv.getData(file);
        }
	if (file == "Err") {
		this->data = "HTTP/1.1 404 OK\r\n";
                this->data += "Content-Type: text/html\r\n";
		this->data += "Connection: close\r\n\r\n";
		this->data += "File " + this->path + " not Found";
	} else { 
		this->data = file; }
	return this->data;
}
void Nanohttp::genericHeader(){
	this->header = "HTTP/1.1 200 OK\r\n";
	this->header += "Access-Control-Allow-Headers: Content-Type, X-Requested-With \r\n";
	this->header += "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS \r\n";
	this->header += "Access-Control-Allow-Origin: * \r\n";
	this->header += "Content-Type: " + getMimeType() + "\r\n";
}
void Nanohttp::sendFile(){}

string Nanohttp::delete_(string data){return "";}
string Nanohttp::connect(string data){return "";}
string Nanohttp::route(string data){
    if (this->path != "/") {
        rest();
    } 
    if (this->path == "/"){
        this->path = "/index.html"; 
    } return this->path;
    return "route";
}

Nanohttp::~Nanohttp(){}