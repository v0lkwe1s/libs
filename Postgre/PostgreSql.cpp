#include "PostgreSql.h"

PostgreSql::PostgreSql() {
    //allow use multiple databases;
    cfg.SetDataBaseDriver("psql");
    //cout << "teste" << endl;
    cfg.SetDatabase();
}

PostgreSql::~PostgreSql() {}

result PostgreSql::select(string sql) {
    try {
        
        connection C(cfg.GetDatabase().c_str());
        
        if (C.is_open()) {
            nontransaction N(C);
            result R(N.exec(sql.c_str()));
            return R;
        }
    } catch (exception& e) {
        cout << e.what() << endl;
    }
    result R;
    return R;
}

string PostgreSql::selectStr(string sql){
    cout << cfg.GetDatabase().c_str() << endl;
    string data= "";
    //try {
        connection C(cfg.GetDatabase().c_str());
        nontransaction N(C);
        result R(N.exec(sql.c_str()));
        for (result::const_iterator c = R.begin(); c != R.end(); ++c) {
            for (unsigned int i = 0; i < c.size(); i++) {
                if (!c[i].is_null()){
                    data += c[i].as<string>()+"|";
                }
                data +="\n";
            }
        }
//    } catch(exception ex) {
//        cout << ex.what() << endl;
//        return "err";
//    }
    return data;
}

void PostgreSql::execute(string sql) {
    try {
        connection C(cfg.GetDatabase());
        if (C.is_open()) {
            work W(C);
            W.exec(sql.c_str());
            W.commit();
            C.disconnect();
        }
    } catch (const std::exception &e) {
            cout << e.what() << endl;
        }
    }


PostgreSql::PostgreSql(const PostgreSql& orig){}