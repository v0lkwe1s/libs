#include "ToJson.h"

ToJson::ToJson() {
}

ToJson::ToJson(const ToJson& orig) {
}

string ToJson::parse(string csv) {
    string ret = "{\"json\":[";
    vector<string> lines = s.split(csv, '\n');
    vector<string> header = s.split(lines.at(0), ';');

    for (unsigned int i = 1; i < lines.size(); i++) {
        for (int j = 0; j < header.size(); j++) {
            vector<string> coll = s.split(lines.at(i), ';');
            if (j == 0) ret += "{";
            ret += "\"" + header.at(j) + "\":";
            if (!s.isNumeric(coll.at(j))) {
                ret += "\"" + coll.at(j) + "\"";
            } else {
                ret += coll.at(j);
            }
            (j == header.size() - 1) ? ret += "}," : ret += ",";
        }
    }
    ret += "]";
    ret = s.replace(ret, ",]", "]}");
    return ret;
}

ToJson::~ToJson() {
}

