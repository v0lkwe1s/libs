#ifndef TOJSON_H
#define TOJSON_H

#include <string>

#include "../strLib/Str.h"
#include <typeinfo>
#include <future>

class ToJson {
public:
    ToJson();
    ToJson(const ToJson& orig);
    string parse(string csv);
    virtual ~ToJson();
private:
    Str s;
    
};

#endif /* TOJSON_H */

