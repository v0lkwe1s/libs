#ifndef CONFIG_H
#define CONFIG_H

#include <string.h>
#include <string>
#include "../strLib/Str.h"

using namespace std;

class Config {
public :
    Config();

    void SetDataBaseDriver(string dataBaseDriver) {this->dataBaseDriver = dataBaseDriver; }
    string GetCertificate() const {return certificate;}
    string GetCertificateKey() const {return certificateKey;}
    string GetSquidLogFile() const {return squidLogFile;}
    string GetWebRootDirectory() const {return webRootDirectory;}
    string GetDataBaseDriver() const {return dataBaseDriver; }
    string GetDataBaseHost() const { return dataBaseHost; }
    void SetDataBaseHost(string dataBaseHost) {this->dataBaseHost = dataBaseHost; }
    string GetDataBaseName() const {return dataBaseName; }
    void SetDataBaseName(string dataBaseName) {this->dataBaseName = dataBaseName;}
    string GetDataBasePasswd() const { return dataBasePasswd;}
    void SetDataBasePasswd(string dataBasePasswd) {this->dataBasePasswd = dataBasePasswd; }
    string GetDataBasePort() const {return dataBasePort; }
    void SetDataBasePort(string dataBasePort) { this->dataBasePort = dataBasePort; }
    string GetDataBaseUser() const { return dataBaseUser; }
    void SetDataBaseUser(string dataBaseUser) { this->dataBaseUser = dataBaseUser;}
    string GetDatabase() const { return database;}
    int GetDebug() const { return debug; }
    void SetDebug(int debug) { this->debug = debug; }

    void SetDatabase() {
        if (this->GetDataBaseDriver() == "psql"){
            this->getDb(GetDataBaseDriver());
            database = "dbname=" + this->GetDataBaseName() + " user=" + this->GetDataBaseUser() + " password=" + this->GetDataBasePasswd() + 
                " hostaddr=" + this->GetDataBaseHost() + " port=" + this->GetDataBasePort() + "";
        }
        if (this->GetDataBaseDriver() == "mysql"){
            this->getDb(GetDataBaseDriver());
            database = "tcp://\"" + this->GetDataBaseHost() + ":" +  this->GetDataBasePort() + ", \"" 
                + this->GetDataBaseUser() + "\", \""  + this->GetDataBasePasswd()+"\"";
        }
    }   
    static bool requireAuth() {
        Str s;
        string file = s.getFileText(s.getCurrentPath()+"/netdebug.conf");
        if (file.find("requireAuth") != std::string::npos) {
            return true;
        }
        return false;
    }
  

private : 
    Str s;
    string webRootDirectory;
    string squidLogFile;
    string certificate;
    string certificateKey;
    string dataBaseName;
    string dataBaseHost;
    string dataBasePort;
    string dataBasePasswd;
    string dataBaseUser;
    string dataBaseDriver;
    string database;
    int debug = 0;
    
    void getDb(string driver);
    
};

#endif /* CONFIG_H */