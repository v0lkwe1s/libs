#include "Config.h"

Config::Config() {
    vector<string> file = s.split(s.getFileText("/opt/netdebug/netdebug.conf"), '\n');
    for (int i = 0; i < file.size(); i++) {
        if (!file[i].find("#") != std::string::npos) {
            if (file[i].find("debug=") != std::string::npos) {
                this->debug = atoi(s.subString(file[i], "debug=").c_str());
            }
            if (file[i].find("squidLog=") != std::string::npos) {
                this->squidLogFile = s.subString(file[i], "squidLog=");
            }
            if (file[i].find("webRootDirectory=") != std::string::npos) {
                this->webRootDirectory = s.subString(file[i], "webRootDirectory=");
            }
            if (file[i].find("certificate=") != std::string::npos) {
                this->certificate = s.subString(file[i], "certificate=");
            }
            if (file[i].find("certificateKey=") != std::string::npos) {
                this->certificateKey = s.subString(file[i], "certificateKey=");
            }

//            if (debug == 10) {
//                cout << file[i] << endl;
//            }
        }
    }
}

void Config::getDb(string driver) {

    vector<string> file = s.split(s.getFileText("/opt/netdebug/netdebug.conf"), '\n');

    for (int i = 0; i < file.size(); i++) {
        cout << file[i] << endl;
        if (file[i].find("databaseDriver=") != std::string::npos) {
            this->SetDataBaseDriver(s.subString(file[i], "databaseDriver="));
        }
        if (this->GetDataBaseDriver() == driver) {
            if (file[i].find("databaseHost=") != std::string::npos) {
                this->SetDataBaseHost(s.subString(file[i], "databaseHost="));
            }
            if (file[i].find("databasePort=") != std::string::npos) {
                this->SetDataBasePort(s.subString(file[i], "databasePort="));
            }
            if (file[i].find("dataBaseName=") != std::string::npos) {
                this->SetDataBaseName(s.subString(file[i], "dataBaseName="));
            }
            if (file[i].find("dataBaseUser=") != std::string::npos) {
                this->SetDataBaseUser(s.subString(file[i], "dataBaseUser="));
            }
            if (file[i].find("databasePasswd=") != std::string::npos) {
                this->SetDataBasePasswd(s.subString(file[i], "databasePasswd="));
            }
        }
    }
}
