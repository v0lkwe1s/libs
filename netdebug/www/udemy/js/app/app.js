angular.module("meuModulo", ['ngRoute'])
    .config(function($routeProvider){
        $routeProvider
            .when("/", {
                templateUrl : "templates/home.html", 
                controller  : "indexController"  
            })
            .when("/contato", {
                templateUrl : "templates/contato.html",
                controller  : "contatoController"
            })
            .otherwise({redirectTo:"/"});
        });