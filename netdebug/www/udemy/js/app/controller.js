angular.module("meuModulo")
.controller("indexController", function($scope){
    $scope.titulo = "Home";
    $scope.alunos = [
        {nome:"Camila", email:"camila@email.com", nota1:65, nota2:90,nota3:55},
        {nome:"Pedro", email:"Pedro@email.com", nota1:65, nota2:80,nota3:55},
        {nome:"Joao", email:"Joao@email.com", nota1:65, nota2:80,nota3:55},
        {nome:"elcio", email:"elcio@email.com", nota1:65, nota2:80,nota3:55},
        {nome:"Jorjana", email:"Jorjana@email.com", nota1:65, nota2:80,nota3:55}
    ];

    var init = function(){
        //limpaForm();
        $scope.alunos.forEach(function(aluno){
           aluno.media = media(aluno);
        });

    };
    var media = function(aluno){
        var media = (parseFloat(aluno.nota1)+parseFloat(aluno.nota2)+parseFloat(aluno.nota3))/3;
        return media.toFixed(2);
    };
    init();
    $scope.addAluno = function(aluno){
        $scope.editando = false;
        aluno.media = media(aluno);
        $scope.alunos.push(aluno);
    }
    $scope.editando = false;
    $scope.editarAluno =function(aluno){
        $scope.editando = true;
        $scope.aluno = aluno;
        $('.modal').modal('open');
    };
    $scope.deletarAluno = function(aluno){
      for (var index in $scope.alunos)  {
          var aux = $scope.alunos[index];
          if (aluno === aux){
              $scope.alunos.splice(index, 1);
          }
      }
    };
    var limpaForm = function(){
        $scope.aluno =  {nome:"", email:"", nota1:'', nota2:'',nota3:''};
    }

})
.controller("contatoController", function($scope){
   $scope.titulo = "Contato"
});