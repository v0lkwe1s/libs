angular.module("netdebug", ['ngRoute'])
    .config(function($routeProvider, $locationProvider, $httpProvider){
        $routeProvider
            .when("/", {
                templateUrl : "templates/home.html", 
                controller  : "indexController"  
            })
            .when("/contato", {
                templateUrl : "templates/contato.html",
                controller  : "contatoController"
            })
            .otherwise({redirectTo:"/"});
            
        });