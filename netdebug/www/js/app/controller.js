angular.module("netdebug")
.controller("indexController", function($scope, $rootScope, $http, $interval){
      
    $interval(function() {
        if ($rootScope.activetab != "home") return;  
        $http.get("json/info/systemLoad")
        .then(function(response) {
            $rootScope.cpuUse = response.data.cpu;
            $rootScope.memTotal = response.data.Load.totalRam;
            $rootScope.memBuffer = response.data.Load.buffers;
            $rootScope.memFree = response.data.Load.freeRam;
            $rootScope.memFreeWbuffer = response.data.Load.freeRam + response.data.Load.buffers;
            
        });
        $http.get("json/ps")
        .then(function(response) {
            $rootScope.proccessList = response.data;
        });
    },3000);
    
  /*$interval(function() {
        $http.get("json/iostat")
        .then(function(response) {
            
            angular.forEach(response.data.iostat, function(item) {
                console.log(item.device + " - Load - "  + item.load);
                
            });
            
        });
    },2000);*/
    

    $scope.reverse = true;
    $scope.sortBy = function(propertyName) {
        $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
        $scope.propertyName = propertyName;
    };
    $rootScope.activetab = "home";
})
.controller("contatoController", function($scope,$rootScope){
    $scope.titulo = "Contato"
    $rootScope.activetab = "contato";
});
