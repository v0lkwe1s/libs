#ifndef SSLSOCKET_H
#define SSLSOCKET_H

#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <string>
#include <arpa/inet.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <cstring>
#include <unistd.h>
#include <iostream>
#include <malloc.h>
#include <fcntl.h>
#include <thread>
#include <sys/wait.h>
#include <thread>
#include "../config/Config.h"
#include "../strLib/Str.h"


using namespace std;

class sslSocket {
public:
    sslSocket();
    sslSocket(string& data);
    sslSocket(const sslSocket& orig);
     
    void init(unsigned int port);
    string getData() const {
        return data;
    }

    void setData(string data) {
        this->data = data;
    }

    void HandleConnections (unsigned int &sock);
    void initSSL(string certificate, string key);
    int send(string data);
    string recv();
    void sslAccept();
    Config cfg;
    char buffer[4096];
    void sslClose();
    virtual ~sslSocket();
    
private:
    unsigned int newsockfd;
    string data;
    int sockfd;
    int n;
    SSL_CTX *ctx;
    SSL *ssl;
    string webRootDirectory;
    string certificate;
    string certificateKey;
    
};

#endif /* SSLSOCKET_H */

