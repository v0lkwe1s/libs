#include "sslSocket.h"

sslSocket::sslSocket() {}

struct sockaddr_in6 serv_addr, cli_addr;

void sslSocket::init(unsigned int port) {
    initSSL(cfg.GetCertificate(), cfg.GetCertificateKey());
    try {
        sockfd = socket(AF_INET6, SOCK_STREAM , 0);
        unsigned int on =1;
        setsockopt( sockfd, SOL_SOCKET, SO_REUSEPORT | SO_REUSEADDR, (const char*) &on, sizeof (on));
        memset((char *) &serv_addr, 0, sizeof(serv_addr));
        serv_addr.sin6_family = AF_INET6;
        serv_addr.sin6_addr = in6addr_any;
        serv_addr.sin6_port = htons(port);
        while (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) == -1 ){
            cout <<  "Trying bind - Control C to abort!\n";
            sleep(1);
        }
        listen(sockfd, 100);
    } catch (exception ex){
        cout << ex.what();
    }
}

void sslSocket::sslAccept(){
        unsigned int len = sizeof(cli_addr);
        newsockfd = accept(sockfd,(struct sockaddr *) &cli_addr,  &len);
        ssl = SSL_new(ctx);
        SSL_set_fd(ssl, newsockfd);
        SSL_accept(ssl);
}

sslSocket::sslSocket(const sslSocket& orig) {}

//void sslSocket::HandleConnections(unsigned int& sock) {
//    if (SSL_accept(ssl) <= 0) {
//        int n = read(sock,buffer,sizeof(buffer));
//        string buff = buffer;
//        n = write(sock, buff.c_str(), buff.length());
//        return;
//        ERR_print_errors_fp(stderr);
//    }
//        send(data);
//        data = "";
//}

int sslSocket::send(string data) {
    return SSL_write(ssl, data.c_str(), data.length()+1);
}
void sslSocket::sslClose(){
    EVP_cleanup();
    SSL_free(ssl);
    close(newsockfd);
}
string sslSocket::recv() {
    int bytes = SSL_read(ssl, buffer, sizeof (buffer));
    return buffer;
}
 void sslSocket::initSSL(string certificate, string key) {
    
    SSL_load_error_strings();
    OpenSSL_add_ssl_algorithms();
    OpenSSL_add_all_algorithms();
    OpenSSL_add_all_ciphers();
    OpenSSL_add_all_digests();

    const SSL_METHOD *method;
    method = SSLv23_server_method();
    ctx = SSL_CTX_new(method);
    SSL_CTX_use_certificate_file(ctx, certificate.c_str(), SSL_FILETYPE_PEM);
    SSL_CTX_use_PrivateKey_file(ctx, key.c_str(), SSL_FILETYPE_PEM);
     
    ERR_print_errors_fp(stderr);
}

sslSocket::~sslSocket() {
    SSL_CTX_free(ctx);
    EVP_cleanup();
    SSL_free(ssl);
}