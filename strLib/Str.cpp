#include "Str.h"

Str::Str() {}
Str::Str(const Str& orig) {}
Str::~Str() {}
string Str::getBashRet(string& comm){
	try {
	FILE *in;
	char buff[512];
	in = popen(comm.c_str(), "r");
	comm = "";
	while (fgets(buff, sizeof(buff), in) != NULL) {
		comm += buff;
	}
	pclose(in);
	} catch (std::bad_alloc& e) {
		cout << e.what() << endl;
		comm = "Err";
	}
	return comm;
}
string Str::getUrlPath(string url){
	vector<string> line;
	line = split(url, '\n');
	vector<string> path;
	path = split(line[0], ' ');
        if (path.size() < 3 ){
            return path[0];
        }
//	//@POG
//	if ((path[1][strlen(path[1].c_str()) - 1]) == '/') {
//            path[1][strlen(path[1].c_str()) - 1] = ' ';
//	}
	path[1] = subString(path[1].c_str(), " ");
	return path[1];
}
void Str::split(const string& s, char c, vector<string>& v) {
    string::size_type i = 0;
    string::size_type j = s.find(c);
    while (j != string::npos) {
        v.push_back(s.substr(i, j - i));
        i = ++j;
        j = s.find(c, j);
        if (j == string::npos) {
            v.push_back(s.substr(i, s.length()));
        }
    }
}
vector<string> Str::split(string str, char delimiter) {
	vector<string> internal;
	stringstream ss(str); // Turn the string into a stream.
	string tok;
        
	while(getline(ss, tok, delimiter)) {
	  internal.push_back(tok);
	}
	return internal;
}
string Str::subString(string t, string s) {
    std::string::size_type i = t.find(s);
    if (i != std::string::npos)
        t.erase(i, s.length());
    return t;
}
string Str::getFileText(string file) {
    string line;
    string text;
    ifstream myfile(file.c_str());
	if (myfile.is_open()) {
            while (!myfile.eof()) {
                getline(myfile, line);
                text+= line+"\n";
            }
        myfile.close();
	} else text = "Err";
	return text;
}
string Str::getCurrentPath(){
	char cwd[1024];
	string path = getcwd(cwd, sizeof(cwd));
	return path;
}
string Str::numberToStr(double n) {
    stringstream ss;
	ss << n;
	return ss.str();
}

void Str::createFileText(string text, string file){
	ofstream out; 
	out.open(file.c_str());
	out << text; 
	out.close(); 
}
/*Retorna ou altera o texto dentro da função, 
 *parametros: \t 
 * subject : texto a ser procurado \n
 * search : texto a ser substituido por replace \n
 */
string Str::replace(string& subject, const string& search, const string& replace){
	size_t pos = 0;
	while ((pos = subject.find(search, pos)) != string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
	return subject;
}
string Str::getTimestamp(){
	time_t t = time(0); 
	struct tm * now = localtime(& t);
	string dt = (numberToStr(now->tm_year + 1900));
	dt += '-';
	dt += (numberToStr(now->tm_mon + 1));
	dt += '-';
	dt += numberToStr(now->tm_mday);
	dt += ' ';
	dt += numberToStr(now->tm_hour);
	dt += ':';
	dt += numberToStr(now->tm_min);
	dt += ':';
	dt += numberToStr(now->tm_sec);
	return dt;
}
void Str::c_buffer_str(char* charBuffer, int RCVBUFSIZE, string& data) {
    for (int i = 2; i < RCVBUFSIZE; i++) {
        data += charBuffer[i];
    }
}
void Str::toUpper(string& str) {
    string buffer =str;
    str="";
    for (int i = 0; i < buffer.length(); i++) {
        str+= toupper(buffer[i]);
    }
}
string Str::upper(string str) {
    string buffer =str;
    for (int i = 0; i < str.length(); i++) {
        buffer+= toupper(str[i]);
    }
    return buffer;
}
const char* Str::sqlitePath() {
    vector<string> config = split(getFileText("netdebug.conf"), '\n');
    string path = "";
    for (unsigned int i = 0; i < config.size(); i++) {
        if ((config[i].find("sqlite")) != std::string::npos) {
            vector<string> colunm = split(config[i], '=');
            path = colunm[1];
        }
    }
    return path.c_str();
}
Str::master Str::toMaster() {
    struct master sock;
    vector<string> config = split(getFileText("netdebug.conf"), '\n');
    for (unsigned int i = 0; i < config.size(); i++) {
        if ((config[i].find("masterHost")) != std::string::npos) {
            vector<string> colunm = split(config[i], '=');
            sock.ip = colunm[1];
        }
        if ((config[i].find("masterPort")) != std::string::npos) {
            vector<string> colunm = split(config[i], '=');
            sock.port = atoi(colunm[1].c_str());
        }
    }
    return sock;
}
