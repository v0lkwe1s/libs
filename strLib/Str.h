#ifndef STR_H
#define STR_H

#include <stdio.h>
#include <vector>
#include <string.h>
#include <sstream>
#include <stdlib.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <limits.h>
#include <unistd.h>

using namespace std;


class Str {
public:
  Str();
  Str(const Str& orig);
  virtual ~Str();
    struct master {
            string ip;
            int port;
        };
    bool isNumeric(string str){
        for (int i = 0; i < str.size(); i++) {
            if (str.at(i) == ',' || str.at(i) == '.') {
                 ;   
            } else if (!isdigit(str.at(i))){
                return false;
            } else {
                ;
            }
        }
        return true;
    }
  void split(const string& s, char c, vector<string>& v);
  vector<string> split(string str, char delimiter);
  static string numberToStr(double n);
  string getFileText(string file);
  string subString(string t, string s);
  void createFileText(string text, string file);
  string replace(string& subject, const string& search,const string& replace);
  string getCurrentPath();
  string getUrlPath(string url);
  string getTimestamp();
  string getBashRet(string& comm);
  void c_buffer_str(char* charBuffer, int RCVBUFSIZE, string& data);
  const char* sqlitePath();
  void toUpper(string& str);
  string upper(string str);
struct master toMaster();

private:
    
};

#endif /* STR_H */
