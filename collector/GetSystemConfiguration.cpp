#include "GetSystemConfiguration.h"

GetSystemConfiguration::GetSystemConfiguration() {
	setHostName();
}
string GetSystemConfiguration::getHostname() {
    return this->hostname;
}
void GetSystemConfiguration::setHostName() {
    string sysReq = "uname -n";
    s.getBashRet(sysReq);
    s.replace(sysReq, "\n", "");
    this->hostname = sysReq;
}
GetSystemConfiguration::GetSystemConfiguration(const GetSystemConfiguration& orig) {}
GetSystemConfiguration::~GetSystemConfiguration() {}
string GetSystemConfiguration::getAll() {
	NetStats nt;
	DiskStats ds;
	const char*  search = "}{";
	const char* replace = ",";
	string buffer = getHost().c_str();
	buffer+= getLoadAvg();
	buffer+= getCpuInfo();
	buffer+=getMemInfo();
	buffer+=getCpuLoad();
	buffer+=getProcessList();
	buffer+= nt.getArpTable();
	buffer+= nt.getIfaces();
	buffer+= nt.getTransferRate();
	buffer += ds.getFileSystems();
	buffer += ds.getDiskLoad();
	string json = s.replace(buffer, search, replace);
	//json[(strlen(json.c_str())-5)] = ' ';
	json = s.replace(json, "\n","");
	return json;
}
string GetSystemConfiguration::getCpuInfo() {
	string sysReq = "lscpu  | awk -F: '{print ""\"\\""\"\"$1\"""\\\": ""\\\"\"$2\"""\\\""",\"""}'";
	string ret = s.getBashRet(sysReq);
	if (ret.length() > 5) ret[(strlen(ret.c_str())-2)] = ' ';
	string cpuInfo = "{\"cpuInfo\":{" + ret + "}}";
	return cpuInfo;
}
string GetSystemConfiguration::getProcessList(){
	string sysReq = "ps --no-headers -aux | awk '{print\"{ \\""\"pid\\""\" : \"$2, \", "
		"\\""\"user\\""\" : \\""\" \" $1 \" \\""\","
		"\\""\"cpu\\""\" : \"  $3  \","
		"\\""\"mem\\""\" : \"  $4  \","
		"\\""\"vsz\\""\" : \"  $5  \","
		"\\""\"rss\\""\" : \"  $6  \","
		"\\""\"tty\\""\" : \\""\" \" $7 \" \\""\","
		"\\""\"stat\\""\" : \\""\" \" $8 \" \\""\","
		"\\""\"start\\""\" : \\""\" \" $9 \" \\""\","
		"\\""\"time\\""\" : \\""\" \" $10 \" \\""\","
		"\\""\"command\\""\" : \\""\" \" $11 \" \\""\"},"
		 "\"}'";
	string json = s.getBashRet(sysReq);
	json[(strlen(json.c_str())-2)] = ' ';
	const string processList = "{\"Proccess\":[" + json  + "]}";
	return processList;
}
string GetSystemConfiguration::getMemInfo(){
	sysinfo (&si);
	string jSon;
	jSon = "{ \"Memory\":{\"totalRam\" : " + s.numberToStr((si.totalram /(1024*1024))) + ","
		"\"freeRam\" : " + s.numberToStr((si.freeram /(1024*1024))) + ","
		"\"buffers\" : " + s.numberToStr((si.bufferram /(1024*1024))) + ","
		"\"shared\" : " + s.numberToStr((si.sharedram)) + ","
		"\"totalSwap\" : " + s.numberToStr((si.totalswap /(1024*1024))) + ","
		"\"freeSwap\" : " + s.numberToStr((si.freeswap /(1024*1024))) + ","
		"\"procs\" : " + s.numberToStr(si.procs) + ""
		"}}";
	return jSon;
}
string GetSystemConfiguration::getCpuLoad(){
	string json;
	long double a[4], b[4], loadavg;
	FILE *fp;

	fp = fopen("/proc/stat","r");
	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3]);
	fclose(fp);
	sleep(1);

	fp = fopen("/proc/stat","r");
	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3]);
	fclose(fp);

	loadavg = ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
	loadavg = loadavg *100;
	
	json = "{\"Load\": {\"cpu\": " + s.numberToStr((double) (loadavg)) + "}}";
		 
	return json;
}

string GetSystemConfiguration::getHost(){
	string sysReq = "uname -n";
	string hostname = s.getBashRet(sysReq);
	sysReq = "uname -sv";
	string kernel = s.getBashRet(sysReq);
	string parse = "{\"Host\" :{\"hostname\" : \"" + hostname + "\","
		"\"kernel\" : \"" + kernel + "\"}}";
	return parse;
}
string GetSystemConfiguration::getLoadAvg(){
	string sysReq = "cat /proc/loadavg  | awk -F \" \" '{print\"{ \\""\"min\\""\" : \"$1, \","
		 "\\""\"min5\\""\" :  \"$2, \","
		 "\\""\"min15\\""\" :  \"$3, \"}"
		 "\"}'";
	string ret = "{\"loadAvg\":";
	ret+= s.getBashRet(sysReq);
	ret+= "}";
	ret = s.replace(ret, "\n","");
	return ret;
}