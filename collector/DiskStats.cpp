#include "DiskStats.h"

DiskStats::DiskStats(){}
DiskStats::DiskStats(const DiskStats& orig){}
string DiskStats::getDiskLoad(){
	string json = "{\"iostat\":[";
	string sysReq = "iostat -x 1 -c 2";
	vector<string> lines = s.split(s.getBashRet(sysReq), '\n');
	vector<string> devices;
	int moreDevices =0;
	for (int i = lines.size() -1; i > 0 ; i--) {
		if (lines[i].find("Device") != std::string::npos){
			break;
		}
		if (lines[i].length() >= 20) {
			devices = (s.split(lines[i], ' '));
			if (moreDevices > 0) json+=",";
			string dev = (devices[devices.size()-1]);
                        dev = s.replace(dev, ",", ".");
			json+="{\"device\" : \"" + devices[0] + "\", \"load\" : " + dev + "}";
			//json+="{\"device\" : \"" + devices[0] + "\", \"load\" : " + devices[devices.size()-1] + "}"; 
			++moreDevices;
		}
	}
	json += "]}";
	return json;
}
string DiskStats::getFileSystems(){
	string comm = "df -Ph |  awk 'NR>1 {print "
		 "\"{\\"
		 "\"fs\\""\": \\""\"\" $1 \"""\\""""\","
		 "\\""\"size\\""\": \\""\"\" $2 \"""\\""""\","
		 "\\""\"used\\""\": \\""\"\" $3 \"""\\""""\","
		 "\\""\"available\\""\": \\""\"\" $4 \"""\\""""\","
		 "\\""\"used%\\""\": \\""\"\" $5 \"""\\""""\","
		 "\\""\"mounted\\""\": \\""\"\" $6 \"""\\""""\""
		 "},\"""}'";

	FILE *in;
	char buff[512];
	in = popen(comm.c_str(), "r");
	string json = "{\"disk\":[";
	while (fgets(buff, sizeof(buff), in) != NULL) {
		json += buff;
	}
	pclose(in);
	json[(strlen(json.c_str()) - 2)] = ' ';
	json += "]}";
	return json;
}
DiskStats::~DiskStats(){}