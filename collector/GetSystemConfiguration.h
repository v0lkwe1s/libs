#ifndef GETSYSTEMCONFIGURATION_H
#define GETSYSTEMCONFIGURATION_H

#include "NetStats.h"
#include "DiskStats.h"
#include <future> 
#include <sys/sysinfo.h>
#include <vector>
#include <fstream>
#include <memory>
#include <iostream>
#include <numeric>
#include <unistd.h>
#include <math.h>
#include "NetStats.h"
#include "DiskStats.h"
#include "../strLib/Str.h"

class GetSystemConfiguration {
public:
    
    GetSystemConfiguration();
    GetSystemConfiguration(const GetSystemConfiguration& orig);
    string getCpuInfo();
    string getMemInfo();
    string getCpuLoad();
    string getProcessList();
    string getHost();
    string getAll();
    string getLoadAvg();
    string getSystemLoad(){
        sysinfo (&si);
        string json = "{ \"Load\":{\"totalRam\" : " + s.numberToStr((si.totalram /(1024*1024))) + ","
		"\"freeRam\" : " + s.numberToStr((si.freeram /(1024*1024))) + ","
		"\"buffers\" : " + s.numberToStr((si.bufferram /(1024*1024))) + ","
		"\"shared\" : " + s.numberToStr((si.sharedram)) + ","
		"\"totalSwap\" : " + s.numberToStr((si.totalswap /(1024*1024))) + ","
		"\"freeSwap\" : " + s.numberToStr((si.freeswap /(1024*1024))) + ","
		"\"procs\" : " + s.numberToStr(si.procs) + ""
		"},";
        long double a[4], b[4], loadavg;
	FILE *fp;
	fp = fopen("/proc/stat","r");
	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3]);
	fclose(fp);
	sleep(1);
	fp = fopen("/proc/stat","r");
	fscanf(fp,"%*s %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3]);
	fclose(fp);
	loadavg = ((b[0]+b[1]+b[2]) - (a[0]+a[1]+a[2])) / ((b[0]+b[1]+b[2]+b[3]) - (a[0]+a[1]+a[2]+a[3]));
	loadavg = loadavg * 100;
	json += "\"cpu\": " + s.numberToStr((double) (loadavg)) + ",";
        json += "\"iostat\":[";
	string sysReq = "iostat -x 1 -c 2";
	vector<string> lines = s.split(s.getBashRet(sysReq), '\n');
	vector<string> devices;
	int moreDevices =0;
	for (int i = lines.size() -1; i > 0 ; i--) {
            if (lines[i].find("Device") != std::string::npos){
                    break;
            }
            if (lines[i].length() >= 20) {
                devices = (s.split(lines[i], ' '));
                if (moreDevices > 0) json+=",";
                string dev = (devices[devices.size()-1]);
                dev = s.replace(dev, ",", ".");
                json+="{\"device\" : \"" + devices[0] + "\", \"load\" : " + dev + "}";
                ++moreDevices;
            }
	}
	json += "]}";
        return json;
    }
    void setHostName();
    string getHostname();
    virtual ~GetSystemConfiguration();

private:
  string hostname;
  const string eof = "}]}";
  struct sysinfo si;
  Str s;
};

#endif /* GETSYSTEMCONFIGURATION_H */

