#ifndef NETSTATS_H
#define NETSTATS_H

#include <stddef.h>
#include <cmath>
#include "../strLib/Str.h"

class NetStats {
public:
  NetStats();
  NetStats(const NetStats& orig);
  virtual ~NetStats();
  
  string getIfaces();
  string getArpTable();
  string getNetDev();
  string getTransferRate();
private:
    Str s;
};

#endif /* NETSTATS_H */

