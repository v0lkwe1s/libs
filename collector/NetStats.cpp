#include "NetStats.h"

NetStats::NetStats(){}
NetStats::NetStats(const NetStats& orig){}
NetStats::~NetStats(){}
//De acordo com a documentação do arp, recomenda-se utilizar o ip neigh para verificar 
// a tabela arp. neste caso estou trazendo todas as conexoes, mas da para filtrar adicionando
// um nud ficando o comando -> ip neighbour show nud reachable
string NetStats::getArpTable(){
	string comm = "ip neighbour show | awk '{print "
		 "\"{\\"
		 "\"host\\""\": \\""\"\" $1 \"""\\""""\","
		 "\\""\"mac\\""\": \\""\"\" $5 \"""\\""""\","
		 "\\""\"iface\\""\": \\""\"\" $3 \"""\\""""\","
		 "\\""\"state\\""\": \\""\"\" $6 \"""\\""""\""
		 "},\"""}'";
	FILE *in;
	char buff[512];
	in = popen(comm.c_str(), "r");
	string json = "{\"arp\":[";
	while (fgets(buff, sizeof(buff), in) != NULL) {
		json += buff;
	}
	pclose(in);
	json[(strlen(json.c_str()) - 2)] = ' ';
	//remove as quebras de linha para enviar tudo via Socket
	json = s.replace(json,"\n\0"," ");
	//Adiciona quebra de linha no final do arquivo, que é para o cliente socket
	// saber que terminou a string
	json += "]}";
        if (json.length() < 20){
            return "";
        }
	return json;
}
string NetStats::getIfaces(){
	string comm = "netstat -i |  awk 'NR>2 {print"
		 "\"{\\"
		 "\"iface\\""\": \\""\"\" $1 \"""\\""""\","
		 "\\""\"MTU\\""\": \\""\"\" $2 \"""\\""""\","
                "\\""\"Met\\""\": \\""\"\" $3 \"""\\""""\","
		 "\\""\"RX-OK\\""\": \\""\"\" $4 \"""\\""""\","
		 "\\""\"RX-ERR\\""\": \\""\"\" $5 \"""\\""""\","
		 "\\""\"RX-DRP\\""\": \\""\"\" $6 \"""\\""""\","
		 "\\""\"RX-OVR\\""\": \\""\"\" $7 \"""\\""""\","
		 "\\""\"TX-OK\\""\": \\""\"\" $8 \"""\\""""\","
		 "\\""\"TX-ERR\\""\": \\""\"\" $9 \"""\\""""\","
		 "\\""\"TX-DRP\\""\": \\""\"\" $10 \"""\\""""\","
		 "\\""\"TX-OVR\\""\": \\""\"\" $11 \"""\\""""\","
		 "\\""\"Flg\\""\": \\""\"\" $12 \"""\\""""\""
		 "},\"""}'";

	FILE *in;
	char buff[512];

	in = popen(comm.c_str(), "r");
	string json = "{\"interfaces\":[";
	while (fgets(buff, sizeof(buff), in) != NULL) {
		json += buff;
	}
	pclose(in);
	json[(strlen(json.c_str()) - 2)] = ' ';
	json += "]}";
	return json;
}
string NetStats::getNetDev(){
	string comm = "cat /proc/net/dev | awk  -F \" \" 'NR >2 {print"
		 "\"{\\"
		 "\"iface\\""\": \\""\"\" $1 \"""\\""""\","
		 "\\""\"RX\\""\": \"$2, \","
		 "\\""\"TX\\""\": \"$10, \""
		 "},\"""}'";
	
	string json = "{\"netUse\":[";
	json += s.getBashRet(comm);
	json[(strlen(json.c_str()) - 2)] = ' ';
	json += "]}";
	return json;
}
string NetStats::getTransferRate(){
	string sysReq = "cat /proc/net/dev | awk  -F \" \" 'NR >2 {print"
	" $1 $2 \":\" $3 }'";
	string comm = sysReq;
	vector<unsigned long long int> rx;
	vector<unsigned long long int> tx;
	string ret = s.getBashRet(comm);
	vector<string> iface = s.split(ret, '\n');
	for (unsigned int i = 0; i < iface.size(); i++) {
		vector<string> rxtx = s.split(iface[i], ':');
		string srx = rxtx[1];
		string stx = rxtx[2];
		rx.push_back(atoi(srx.c_str()));
		tx.push_back(atoi(stx.c_str()));
	}
	usleep(500);
	comm = sysReq;	
	ret = s.getBashRet(comm);
	iface = s.split(ret, '\n');
	string json ="{\"netTransferRate\":[";
	for ( unsigned int i = 0; i < iface.size(); i++) {
		vector<string> rxtx = s.split(iface[i], ':');
		string srx = rxtx[1];
		string stx = rxtx[2];
		unsigned long long int brx = atoi(srx.c_str());
		unsigned long long int btx = atoi(stx.c_str());
		btx = btx - tx[i];
		brx = brx - rx[i];
		json += "{\"iface\":\"" + rxtx[0] + "\",";
		json += "\"rx\":" + s.numberToStr(round(2*(brx/1024))) + ",";
		json += "\"tx\":" + s.numberToStr(round(2*(btx/1024))) + "},";
	}
	json += "]}";
	s.replace(json, ",]}" , "]}");
	return json;
}