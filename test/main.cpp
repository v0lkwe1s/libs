#include <cstdlib>
#include "../Postgre/PostgreSql.h"
#include "../SSlSocket/sslSocket.h"
#include "../strLib/Str.h"
#include "../config/Config.h"
#include <stdio.h>
#include "../NanoHttp/Nanohttp.h"
#include "../jsonCsv/ToJson.h"
#include "../collector/DiskStats.h"
#include "../collector/GetSystemConfiguration.h"

using namespace std;
Str s;
Config cfg;
string json(vector<string> uri);
string response(string data);

int main(int argc, char** argv) {
    
    sslSocket sock;
    sock.init(5555);
    int clients =0;
    
    while (clients < 10000) {
        pid_t pid =  fork();
        string data = "";
         clients++;
                sock.sslAccept();
                Nanohttp nh;
                data = sock.recv();
                vector<string> line = s.split(data, '\n');
                if (data.find("/json/") == string::npos) {
                    data = nh.shttp(data);
                    sock.send(data);
                    data = "";
                } else {
                    vector<string> col = s.split(line.at(0), ' ');
                    if (col.size() > 1) {
                        vector<string> uri = s.split(col.at(1), '/');
                        for (int i = 0; i < uri.size(); i++) {
                            if (uri.at(i) == "json"){
                                data = json(uri);
                                sock.send(data);
                                data = "";
                            }
                        }
                    }
                }
                
        switch (pid) {
            case 0 : {
                printf("Parent: %d This Pid: %d \n", getpid(), getppid());
            }
            case -1 : {
                perror("Error fork");
                exit(EXIT_SUCCESS);
            }
            default :  {
                clients--;
                signal(SIGHUP, SIG_IGN);
                signal(SIGCHLD, SIG_IGN);
                signal(SIGPIPE, SIG_IGN);
            }
        }
        
    }      
    
    cout << "Terminou os clientes "<< endl; 
    
    
        
//    PostgreSql ps;
//    //Config cfg;
//    
//    
//    result rs = ps.select("select table_name from information_schema.tables"
//            " where table_name not like '%pg_%'"
//            " and table_schema not like '%_pg_%' "
//            "and table_schema not in ('information_schema')");
//    for (result::const_iterator c = rs.begin(); c != rs.end(); ++c) {
//        for (unsigned int i = 0; i < c.size(); i++) {
//            if (!c[i].is_null()){
//                result col = ps.select("select * from information_schema.columns "
//                        "where table_name = '" + c[i].as<string>() +"'");
//                for (result::const_iterator d = col.begin(); d != col.end(); ++d) {
//                    for (unsigned int j = 0; j < d.size(); j++) {
//                        if (!d[j].is_null()) cout << d[j].as<string>() << endl;
//                    }
//                }
//            }
//        }
//    }
    
    return 0;
}

string json(vector<string> uri){
    string data ="";
    if (uri.size() == 2) {
        return response("{[]}");
    }
    if (uri.at(1) == "json" && uri.at(2) == "ps") {
        string com = ("ps -aux | awk -F \" \" '{print $1\";\"$2\";\"$3\";\"$4\";\"$7\";\"$8\";\"$9\";\"$10\";\"$11}'");
        ToJson tj;
        string ps = tj.parse(s.getBashRet(com));
        ps = s.replace(ps,"%","");
        return response(ps);
    }
    if (uri.at(1) == "json" && uri.at(2) == "info") {
        GetSystemConfiguration gs;
        if (uri.size() == 3) {return response(gs.getAll()); }
        if (uri.at(3) == "mem") {return response(gs.getMemInfo()); }
        if (uri.at(3) == "iostat") {DiskStats ds; return response(ds.getDiskLoad()); } 
        if (uri.at(3) == "cpuInfo") { return response(gs.getCpuInfo()); }
        if (uri.at(3) == "cpuLoad") { return response(gs.getCpuLoad()); }
        if (uri.at(3) == "systemLoad") { return response(gs.getSystemLoad()); }
    }
    return response("{[]}");
}
string response(string data){
    string header = "HTTP/1.1 200 OK\r\n";
    header += "Access-Control-Allow-Headers: Content-Type, X-Requested-With \r\n";
    header += "Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS \r\n";
    header += "Access-Control-Allow-Origin: * \r\n";
    header += "Content-Type: application/json\r\n";
    header += "Content-Length: " + s.numberToStr(data.length())  +  " \r\n";
    header += "Connection: close\r\n\r\n";
    return header+data;
}