#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/main.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-lpqxx -lpq -lpthread -g --std=c++11 -lcrypto -lssl -fPIC
CXXFLAGS=-lpqxx -lpq -lpthread -g --std=c++11 -lcrypto -lssl -fPIC

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=../config/dist/Debug/GNU-Linux/libconfig.a ../Postgre/dist/Debug/GNU-Linux/libpostgre.a ../SSlSocket/dist/Debug/GNU-Linux/libsslsocket.a ../strLib/dist/Debug/GNU-Linux/libstrlib.a ../NanoHttp/dist/Debug/GNU-Linux/libnanohttp.a ../jsonCsv/dist/Debug/GNU-Linux/libjsoncsv.a /home/deb/Dropbox/c++/collector/dist/Debug/GNU-Linux/libcollector.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/test

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/test: ../config/dist/Debug/GNU-Linux/libconfig.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/test: ../Postgre/dist/Debug/GNU-Linux/libpostgre.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/test: ../SSlSocket/dist/Debug/GNU-Linux/libsslsocket.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/test: ../strLib/dist/Debug/GNU-Linux/libstrlib.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/test: ../NanoHttp/dist/Debug/GNU-Linux/libnanohttp.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/test: ../jsonCsv/dist/Debug/GNU-Linux/libjsoncsv.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/test: /home/deb/Dropbox/c++/collector/dist/Debug/GNU-Linux/libcollector.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/test: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/test ${OBJECTFILES} ${LDLIBSOPTIONS} -s

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -g -include ../strLib/Str.h -include ../Postgre/PostgreSql.h -include ../config/Config.h -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

# Subprojects
.build-subprojects:
	cd ../config && ${MAKE}  -f Makefile CONF=Debug
	cd ../Postgre && ${MAKE}  -f Makefile CONF=Debug
	cd ../SSlSocket && ${MAKE}  -f Makefile CONF=Debug
	cd ../strLib && ${MAKE}  -f Makefile CONF=Debug
	cd ../NanoHttp && ${MAKE}  -f Makefile CONF=Debug
	cd ../jsonCsv && ${MAKE}  -f Makefile CONF=Debug
	cd /home/deb/Dropbox/c++/collector && ${MAKE}  -f Makefile CONF=Debug

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:
	cd ../config && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../Postgre && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../SSlSocket && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../strLib && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../NanoHttp && ${MAKE}  -f Makefile CONF=Debug clean
	cd ../jsonCsv && ${MAKE}  -f Makefile CONF=Debug clean
	cd /home/deb/Dropbox/c++/collector && ${MAKE}  -f Makefile CONF=Debug clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
